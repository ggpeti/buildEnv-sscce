{
  description = "SSCCE for collision behavior of buildEnv";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: {
      defaultPackage = nixpkgs.legacyPackages.${system}.buildEnv {
        name = "test";
        paths = [ ./a ];
        pathsToLink = [ "/b" ];
      };
    });
}
